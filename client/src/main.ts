import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication, BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app/app.component';
import { HttpClientModule } from "@angular/common/http";
import { environment } from './environments/environment';
import { appRoutes } from './app/app.routes';


if (environment.production) {
  enableProdMode();
  window.console.log = () => { }
}


bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(
      RouterModule.forRoot(appRoutes),
      HttpClientModule,
      BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    )
  ]
})
  .catch(err => console.error(err));

