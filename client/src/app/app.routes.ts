import { AppComponent } from './app.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    component: AppComponent,
    // children: [
    //   {
    //     path: 'home',
    //     loadChildren: () => import('')
    //       .then((m) => m.HOME_ROUTES)
    //   },
    // ],
  },
  // { path: '**', redirectTo: '/home' },
];



